#!/bin/bash

# exit on failure
# set -e
# set -o pipefail
# set -o nounset

init_expw() {
    local -r basedir="$(realpath $(dirname $BASH_SOURCE))"
    source "${basedir}/dependency.sh" || { printf "Could not source dependency check library. Aborting.\n"; exit 1; }
    source "${basedir}/log.sh" || { printf "Could not source log library. Aborting.\n"; exit 1; }
    source "${basedir}/expw_configuration.sh" || { log_critical "Could not source configuration library. Aborting."; exit 1; }
    declare -gr PERF_BIN="$(which perf)"
    declare -gr EXPW_DEPENDENCIES="perf date jq grep awk paste sed head"
}

perf_wrapper() {
    $PERF_BIN stat --event="$EXPW_PERF_EVENTS" --output="$EXPW_OUT_TEMP_PERF" --append --field-separator="," --interval-print="$EXPW_PERF_INTERVAL" "$@"
}

get_variable_values() {
    local values=()
    local return_value=0
    for variable_name in $(jq -r '.[].name' <<< $EXPW_VARIABLES); do
        if variable_value="$($(jq -r --arg name ""$variable_name"" '.[] | select(.name == $name).value' <<< $EXPW_VARIABLES))"; then
            log_info "Variable %s=%s" "$variable_name" "$variable_value"
            values+=("$variable_value")
        else
            log_error "Could not get value for variable %s" "$variable_name"
            return_value=1
        fi
    done
    (IFS=,; printf "${values[*]}")
    return $return_value
}

get_constant_values() {
    local values=()
    local return_value=0
    for constant_name in $(jq -r '.[].name' <<< $EXPW_CONSTANTS); do
        if constant_value="$(jq -r --arg name ""$constant_name"" '.[] | select(.name == $name).value' <<< $EXPW_CONSTANTS)"; then
            log_info "Constant %s=%s" "$constant_name" "$constant_value"
            values+=("$constant_value")
        else
            log_error "Could not get value for constant %s" "$constant_name"
            return_value=1
        fi
    done
    (IFS=,; printf "${values[*]}")
    return $return_value
}

wrapper() {
    log_info "\`start\` variables:"
    variable_results="$(get_variable_values)" || log_error "Could not get all \`start\` variable values."
    perf_wrapper "$@" || log_error "perf returned a non-zero value: %d" "$?"
    log_info "\`end\` variables:"
    variable_end="$(get_variable_values)" || log_error "Could not get all \`end\` variable values."
    [[ -n "$variable_results" && -n "$variable_end" ]] && variable_results+=",$variable_end"
    [[ -z "$variable_results" ]] && variable_results="$variable_end"
}

perf_tidy() {
    local return_value=0
    if local perf_results="$(grep -P '^\s+\d+' ${EXPW_OUT_TEMP_PERF} | head -1 | sed -E 's/\s+([0-9]+.[0-9]+),.*/\1/')" \; then
        log_error "Could not get real execution time from perf."
        return_value=1
    fi
    if local perf_events_results="$(grep -P '^\s+\d+' ${EXPW_OUT_TEMP_PERF} | awk -F '"*,"*' '{print $2}' | paste -sd ',' -)"; then
        log_error "Could not get perf events results"
        return_value=1
    fi
    [[ -n "$perf_events_results" ]] && perf_results+=",$perf_events_results"
    printf "%s\n" "$perf_results"
    return $return_value
}

header_variable() {
    local header_variable_names=()
    for variable in $(jq -r '.[].name' <<< $EXPW_VARIABLES); do
        header_variable_names+=("\"${variable}-$1\"")
    done
    (IFS=,; printf "${header_variable_names[*]}")
}

write_header() {
    local header="execution-time(s)"
    [[ -n "${EXPW_PERF_EVENTS:-}" ]] && header+=",$EXPW_PERF_EVENTS"
    local constant_header="$(jq -r 'map(.name) | @csv' <<< $EXPW_CONSTANTS)" \
          || log_error "Could not get constant names for the header"
    [[ -n "$constant_header" ]] && header+=",$constant_header"
    local variable_start_header="$(header_variable start)"
    [[ -n "$variable_start_header" ]] && header+=",$variable_start_header"
    local variable_end_header="$(header_variable end)"
    [[ -n "$variable_end_header" ]] && header+=",$variable_end_header"
    printf "%s\n" "$header" > "$EXPW_OUT_RESULTS"
    log_info "Wrote header `%s` to %s" "$header" "$EXPW_OUT_RESULTS"
}

write_results() {
    [[ ! -s "$EXPW_OUT_RESULTS" ]] && write_header
    local results=""
    local perf_results="$(perf_tidy)" || log_error "Could not retrieve all perf results"
    [[ -n "$perf_results" ]] && results+="$perf_results"
    local constant_results="$(get_constant_values)" || log_error "Could not retrieve all constant values"
    [[ -n "$constant_results" ]] && results+=",$constant_results"
    [[ -n "$variable_results" ]] && results+=",$variable_results"
    printf "%s\n" "$results" >> "$EXPW_OUT_RESULTS"
    log_info "Wrote results to %s" "$EXPW_OUT_RESULTS"
}

print_usage() {
    printf "This wrapper can be configured with a JSON configuration in\n"
    printf " the EXPW_CONFIGURATION. This variable can contain a path to a json file\n"
    printf " or the configuration itself."
    ## TODO: json format
    printf "The following environment variables have priority over the JSON configuration:\n"
    printf " EXPW_OUT_FOLDER Path where experiments results and temporary\n"
    printf "                 files will be saved. Default:\n"
    printf "                 \`\$HOME/expw_out/(timestamp)\`\n"
    printf " EXPW_OUT_RESULTS Results file. Default:\n"
    printf "                  \`\$EXPW_OUT_FOLDER/${EXPW_OUT_RESULTS_FILENAME}\`\n"
    printf " EXPW_OUT_TEMP_PERF Temporary file for perf results. Default:\n"
    printf "                    \`\$EXPW_OUT_FOLDER/perf_temp.csv\`\n"
    printf " Please note that (timestamp) has the following \`date\` format:\n"
    printf "                  %%s.%%N (seconds since epoch.nanoseconds)\n"
    printf " EXPW_PERF_EVENTS List of performance events to monitor. Default:\n"
    printf "                  ${EXPW_PERF_EVENTS_DEFAULT} (always activated)\n"
    printf "                  Note: cpu-clock measures the user time across all threads.\n"
    ## TODO: constants and variables
    printf " All control ouput is logged to:\n"
    printf "                 \`\$HOME/expw_out/${SCRIPT_NAME}.log\`\n"
}

display_usage() {
    printf "Usage: ${SCRIPT_NAME}\n"
    print_usage
}

main() {
    check_dependencies "$EXPW_DEPENDENCIES"
    [[ -z "$@" ]] && display_usage && exit 0
    expw_configure || { log_critical "Could not configure expw. Aborting."; exit 1; }
    log_info "Command line: %s" "$*"
    wrapper "$@"
    write_results
}

main "$@"
